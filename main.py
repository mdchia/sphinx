def dosomething(a, b):
    """The dosomething function can do something with the numbers. In this case, I'm just adding them.

    :param a: The first parameter
    :type a: Not specified (not None)
    :param b: The second parameter
    :type b: Not specified (not None)
    :raises ValueError: ValueError if a or b are None
    :return: Returns a+b
    :rtype: Determined by the type of the sum of the parameters
    """
    if a is None:
        raise ValueError('a is None')
    if b is None:
        raise ValueError('b is None')
    return a + b


if __name__ == '__main__':
    """Note that Sphinx *imports* your code - so it's good practice not to let any code "leak"
    - i.e. by importing, Sphinx executes anything not wrapped in a function, object, or
    the statement: if __name__ == '__main__':
    """

    print(dosomething(1, 2))
